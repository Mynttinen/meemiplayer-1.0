<!--Etsii PHP:lla halutusta kansiosta kaikki videotiedostojen nimet!-->
<?php

$files = scandir ( "videot/"); //Videoiden kansio



$randomFile = $files[shuffle($files)]; //Randomisoi videon

?>

<!DOCTYPE html>
<html>
<head>
<title>Randomitoistin</title>
<link rel="stylesheet" type="text/css" href="tyylit.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
</head>
<body>
<div class="container">
<h1>MP4-kansion helmet ja vähemmän helmet :D</h1>

<div class="d-flex justify-content-center">
<video onclick="this.paused? this.play() : this.pause()" onloadstart="this.volume=0.25" id="player" autoplay controls> <!-- Videotiedoston volume muutettu puolilleen ja säädetty klikkaamalla pause ja resume-->
  <source src="videot/<?php echo $randomFile; ?>" type="video/mp4"> <!-- Videotiedoston polku tulostetaan -->
  
Your browser does not support the video tag.
</video>
</div>
<div class="d-flex justify-content-center">
<h3>Tiedostonimi: <?php echo $randomFile; ?></h3> <!-- Videotiedoston nimi tulostetaan -->
</div>
<!-- Suorittaa sivun uudelleen latauksen videon skippaamiseksi -->
<button type="button" class="btn btn-danger btn-lg btn-block" onClick="window.location.reload()">Skippaa video</button> 

</div>

<!-- Javascript pätkä, joka lataa sivun uudelleen videotiedoston toistamisen loputtua -->
<script type='text/javascript'>

document.getElementById('player').addEventListener('ended', function(e) {

    location.reload();

})

</script>
</body>
</html>