# MeemiPlayer 1.0
Eräänä iltana sain kavereiden innoittamana ajatuksen, että videotiedostoja palvelimellani olevasta salatusta kansiosta täytyisi pystyä katsomaan täysin randomisti ilman erikoisempia toimenpiteitä! Joten tehtäväksi muodostui siis lokaalistitallennettujen meemivideoiden katsomiseen tarkoitettu toistin.

## Yleistä toistimesta
* PHP:tä käytetään hakemaan videoiden polku ja löydetyistä tiedostoista arvotaan näytettävä tiedosto.

* Javascriptillä on toteutettu myös videon loppuessa tai "Skippaa video" -nappia painaessa sivun pakotettu uudelleen lataaminen, joka mahdollistaa seuraavan randomin videon toistamisen.

* HTML5 playeri toimii videoiden toistamisessa. Playeriin on tehty oletuksena muutoksia:
1. Videon pysäyttäminen videota painamalla.
2. Videoiden voimakkuutta on pienennetty 25%, ehkäisemään äänen "räjähdykset".
3. Lisäksi lisätty autoplay ja sallittu normaalit kontrollit.

Videotoistimen vähäiset tyylittelyt on toteutettu tähän malliversioon Bootstrap 4:llä.

### Havaitut bugit
Ainoa havaittu bugi tällähetkellä on, että array saa mukaan välillä .-nimisen tiedoston. Bugi on helppo huomata pienellä tiedostomäärällä!